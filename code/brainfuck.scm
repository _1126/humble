(use miscmacros extras (srfi 1 4 13))

(define-record vm
  (setter idx)
  (setter pc)
  (setter program)
  (setter paren-stack)
  (setter tape))

(define-record-printer (vm v p)
    (fprintf p
           "#,<vm pc ~s data ~s (~s) stack ~s>"
           (vm-pc v)
           (vm-idx v)
           (u8vector-ref (vm-tape v) (vm-idx v))
           (vm-paren-stack v)))

(define mem-size (make-parameter 30000))
(define debug? (make-parameter #f))

(define (init-machine!)
  (make-vm 0 0 "" '() #f))

(define (load-from-file file)
  (let ((code
         (string-concatenate
          (with-input-from-file file read-lines)))
        (vm (init-machine!)))
    (set! (vm-tape vm) (make-u8vector (mem-size) 0))
    (set! (vm-program vm) code)
    vm))

(define (find-loop-start pc prog)
  (let loop ((p pc)
             (open-parens 0))
    (cond ((< open-parens 0)
           (error "One [ too many in " (sub1 p)))
          ((< p 0)
           (error "] without matching ["))
          ((equal? (string-ref prog p)
                   #\])
           (loop (sub1 p)
                 (add1 open-parens)))
          ((equal? (string-ref prog p)
                    #\[)
           (if (= 0 (sub1 open-parens))
               p
               (loop (sub1 p)
                     (sub1 open-parens))))
          (else (loop (sub1 p) open-parens)))))

(define (find-loop-end pc prog)
  (let loop ((p pc)
             (open-parens 0))
    (cond ((< open-parens 0)
           (error "One ] too many in " (sub1 p)))
          ((> p (sub1 (string-length prog)))
           (error "Unexpected end of file"))
          ((equal? (string-ref prog p)
                   #\[)
           (loop (add1 p)
                 (add1 open-parens)))
          ((equal? (string-ref prog p)
                    #\])
           (if (= 0 (sub1 open-parens))
               p
               (loop (add1 p)
                     (sub1 open-parens))))
          (else (loop (add1 p) open-parens)))))

(define (move-right vm)
  (let ((new vm))
    (inc! (vm-idx new))
    new))

(define (move-left vm)
  (let ((new vm))
    (dec! (vm-idx new))
    new))

(define (change-curr-cell vm prod)
  (let* ((new vm)
         (curr (u8vector-ref (vm-tape new) (vm-idx new))))
    (u8vector-set! (vm-tape new) (vm-idx new) (modulo (prod curr) 256))
    new))

(define (inc-curr-cell vm) (change-curr-cell vm add1))
(define (dec-curr-cell vm) (change-curr-cell vm sub1))

(define (print-curr-cell vm)
  (display (integer->char (u8vector-ref (vm-tape vm) (vm-idx vm))))
  (flush-output)
  vm)

(define (read-char-in vm)
  (let* ((new vm)
         (c (read-char)))
    (unless (eof-object? c)
            (u8vector-set! (vm-tape new) (vm-idx new) (char->integer c)))
    new))

(define (handle-loop-start vm)
  (let ((new vm))
    (push! (vm-pc new) (vm-paren-stack new))
    (when (zero? (u8vector-ref (vm-tape new) (vm-idx new)))
          (set! (vm-pc new) (sub1 (find-loop-end (vm-pc new) (vm-program new)))))
    new))

(define (handle-loop-end vm)
  (let ((new vm))
    (if (not (zero? (u8vector-ref (vm-tape new) (vm-idx new))))
        (set! (vm-pc new) (car (vm-paren-stack new)))
        (pop! (vm-paren-stack new)))
    new))

(define (print-vm-state vm)
  (when (debug?)
        (display vm (current-error-port))
        (newline (current-error-port)))
  vm)

(define (bf-eval m)
  (let ((action
         (case (string-ref (vm-program m) (vm-pc m))
           ((#\>) move-right)
           ((#\<) move-left)
           ((#\+) inc-curr-cell)
           ((#\-) dec-curr-cell)
           ((#\.) print-curr-cell)
           ((#\,) read-char-in)
           ((#\[) handle-loop-start)
           ((#\]) handle-loop-end)
           ((#\#) print-vm-state)
           (else identity)))
        (next-pc (add1 (vm-pc m))))
    (when (< next-pc
           (string-length (vm-program m)))
          (set! (vm-pc m) next-pc)
          (bf-eval (action m)))))


